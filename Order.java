public class Order {
	
	public static final int MAX_NUMBERS_ORDERED = 3;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("full -> ko the add them");
		}else {
			this.itemsOrdered[this.qtyOrdered++] = disc;
			System.out.println("add thanh cong "+ disc.getTitle());
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for(int i=0; i<qtyOrdered; i++) {
			if(this.itemsOrdered[i] == disc) {
				for(int j=i; j < qtyOrdered-1; j++) {
						this.itemsOrdered[j] = this.itemsOrdered[j+1];
			}
			this.qtyOrdered-=1;
			System.out.println("remove thanh cong "+disc.getTitle());
			return;
			}
		}
		System.out.println("remove error");
	}
	
	public float totalCost() {
		float total = 0;
		for(int i=0; i<this.qtyOrdered; i++) {
			total += this.itemsOrdered[i].getCost();
		}
		return total;
	}
	
	public Order() {
		this.qtyOrdered = 0;
	}
}