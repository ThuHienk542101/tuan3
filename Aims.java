
public class Aims {
	public static void main(String[] args) {
		
		Order anOrder = new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		//System.out.println(dvd1.getTitle() + "\n" + dvd1.getCategory() + "\n" + dvd1.getDirector() + "\n" + dvd1.getLength() + "\n" + dvd1.getCost());
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);
		
		anOrder.addDigitalVideoDisc(dvd1);
		anOrder.addDigitalVideoDisc(dvd2);
		anOrder.addDigitalVideoDisc(dvd3);
		//anOrder.addDigitalVideoDisc(dvd2);
		anOrder.removeDigitalVideoDisc(dvd2);
		//anOrder.removeDigitalVideoDisc(dvd1);
		anOrder.removeDigitalVideoDisc(dvd3);
		//System.out.println("Total cost = "+ anOrder.totalCost());
		System.out.println("Total cost = " +anOrder.totalCost());
	}
}
